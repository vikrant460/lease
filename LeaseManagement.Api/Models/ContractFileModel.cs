﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LeaseManagement.Api.Models
{
    public class ContractFileModel
    {
        public int Id { get; set; }
        public IFormFile File { get; set; }
    }
}
