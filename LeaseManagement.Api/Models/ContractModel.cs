﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LeaseManagement.Api.Models
{
    public class ContractModel
    {
        [Required]
        public int Id { get; set; }

        public int OrganizationId { get; set; }

        public IEnumerable<ContractDocumentModel> ContractDocuments { get; set; }
        public IEnumerable<ActionModel> Actions { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        public int StatusId { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime LastUpdateDate { get; set; }

        public Guid CreatedBy { get; set; }
        public Guid LastUpdatedBy { get; set; }

    }
}
