﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaseManagement.Api.Models
{
    public class ActionModel
    {
        public int Id { get; set; }
        public int ContractId { get; set; }
        public int ContractDocumentId { get; set; }
        public int ContractScratchDocumentId { get; set; }
        public int StatusId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime ActionDate { get; set; }

    }
}
