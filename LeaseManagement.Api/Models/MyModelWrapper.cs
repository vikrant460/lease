﻿using LeaseManagement.Api.ModelBinder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaseManagement.Api.Models
{
    public class MyModelWrapper
    {
        public IList<IFormFile> Files { get; set; }
        [FromJson]
        public ContractModel Model { get; set; } // <-- JSON will be deserialized to this object
    }
}
