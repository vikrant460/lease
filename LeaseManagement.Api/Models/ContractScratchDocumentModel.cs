﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaseManagement.Api.Models
{
    public class ContractScratchDocumentModel
    {
        public int Id { get; set; }
        public int ContractId { get; set; }
        public int ContractDocumentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }        
        public int Seq { get; set; }
        public int StatusId { get; set; }
        public int Version { get; set; }
    }
}
