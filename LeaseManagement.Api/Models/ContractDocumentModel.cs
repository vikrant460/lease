﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaseManagement.Api.Models
{
    public class ContractDocumentModel
    {
        public int Id { get; set; }
        public int ContractId { get; set; }
        public ICollection<ContractScratchDocumentModel> ContractScratchDocuments { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }

        public int Seq { get; set; } //Will decide how the versions are ordered 

        public int StatusId { get; set; }

    }
}
