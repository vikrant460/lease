﻿using LeaseManagement.Api.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LeaseManagement.Api.Services
{
    public interface ILeaseManagementDataService
    {
        Task<IEnumerable<ContractModel>> GetAllContracts();
        Task SaveContract(ContractModel contract);
    }

    public class DummyLeaseManagementDataService : ILeaseManagementDataService
    {
        private List<ContractModel> _contracts;
        public DummyLeaseManagementDataService()
        {
            var contractsdata = File.ReadAllText(@"Data\dummycontracts.json");
            var contracts = JsonConvert.DeserializeObject<List<ContractModel>>(contractsdata);
            _contracts = contracts;
        }

        public async Task<IEnumerable<ContractModel>> GetAllContracts()
        {
            return _contracts;
        }

        public async Task SaveContract(ContractModel contract)
        {
            _contracts.Add(contract);
            File.WriteAllText(@"Data\dummycontracts.json", JsonConvert.SerializeObject(_contracts));

            // serialize JSON directly to a file
            using (StreamWriter file = File.CreateText(@"Data\dummycontracts.json"))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, _contracts);
            }
        }


    }
}
