﻿using LeaseManagement.Api.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaseManagement.Api.Repositories.Interfaces
{
    public interface IContractRepository
    {
        Task<IEnumerable<ContractModel>> GetAllContracts();
        Task SaveContract(ContractModel contract);
        Task UploadDocument(IList<IFormFile> formFiles, string targetPath, int version = 1);
    }
}
