﻿using LeaseManagement.Api.Helper;
using LeaseManagement.Api.Models;
using LeaseManagement.Api.Repositories.Interfaces;
using LeaseManagement.Api.Services;
using LeaseManagement.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LeaseManagement.Api.Repositories
{
    public class ContractRepository : IContractRepository
    {
        private ContractContext _contractConext;
        private readonly ILeaseManagementDataService _dataService;

        public ContractRepository(ContractContext dbContext, ILeaseManagementDataService dataService)
        {
            _contractConext = dbContext;
            _dataService = dataService;
        }

        public async Task<IEnumerable<ContractModel>> GetAllContracts()
        {
            var contracts =  await _dataService.GetAllContracts();
            return contracts;
        }

        public async Task SaveContract(ContractModel contract)
        {
            await _dataService.SaveContract(contract);
        }

        public async Task UploadDocument(IList<IFormFile> formFiles, string targetPath, int version = 1)
        {
            Directory.CreateDirectory(targetPath);

            
            foreach (var file in formFiles)
            {
                var filePath = Path.Combine(targetPath, $"{Path.GetFileNameWithoutExtension(file.FileName)}_v{version}.{Path.GetExtension(file.FileName)}");
                if (File.Exists(filePath))
                {
                    Interlocked.Increment(ref version);
                }
                await FileHelper.SaveFile(file, targetPath, version);

            }
        }
    }
}
