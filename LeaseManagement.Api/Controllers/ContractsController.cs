﻿using BrunoZell.ModelBinding;
using LeaseManagement.Api.Helper;
using LeaseManagement.Api.Models;
using LeaseManagement.Api.Repositories.Interfaces;
using LeaseManagement.Api.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LeaseManagement.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ContractsController : ControllerBase
    {
        private ILeaseManagementDataService _dataService;
        private readonly string _targetFilePath;
        private readonly IContractRepository _contractRepository;

        public ContractsController(ILogger<ContractsController> logger, IConfiguration config, IContractRepository contractRepository)
        {
            _targetFilePath = config.GetValue<string>(FileHelper.STOREDFILEPATHKEY);
            _contractRepository = contractRepository;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ContractModel>>> GetContracts()
        {
            try
            {
                //TODO fetch from database
                var result = await _contractRepository.GetAllContracts();
                return result.ToArray();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,ex.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult<ContractModel>> SaveContract(ContractModel contract)
        {
            try
            {
                //TODO save database 
                await _contractRepository.SaveContract(contract);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost("upload")]
        public async Task<ActionResult<ContractDocumentModel>> UploadContractDocument([ModelBinder(BinderType = typeof(JsonModelBinder))] ContractDocumentModel contractDocument, IList<IFormFile> files)
        {
            try
            {
                //TODO save to database
                if (files == null || contractDocument == null)
                {
                    return BadRequest("Invalid request");
                }
                var path = Path.Combine(_targetFilePath, contractDocument.Name);
                if (contractDocument.ContractScratchDocuments?.Count > 0)
                {
                    foreach (var scratchDoc in contractDocument.ContractScratchDocuments)
                    {
                        
                        await _contractRepository.UploadDocument(files, path, scratchDoc.Version);
                        scratchDoc.Version++;
                    }
                }
                else 
                {
                    await _contractRepository.UploadDocument(files, path);
                }
               
                return contractDocument;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

    }
}
