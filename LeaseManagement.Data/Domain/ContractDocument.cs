﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaseManagement.Data.Domain
{
    public class ContractDocument : Timestamp
    {
        public ContractDocument()
        {
            ContractScratchDocuments = new List<ContractScratchDocument>();
            Actions = new List<Action>();
        }
        public Guid Id { get; set; }

        public Contract Contract { get; set; }
        public ICollection<ContractScratchDocument> ContractScratchDocuments { get; set; }

        public ICollection<Action> Actions { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }

        public int Seq { get; set; }

    }
}
