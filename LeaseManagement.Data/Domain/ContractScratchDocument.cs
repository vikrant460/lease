﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaseManagement.Data.Domain
{

    public class ContractScratchDocument : Timestamp
    {
        public ContractScratchDocument()
        {
            Actions = new List<Action>();
        }

        public Guid Id { get; set; }

        public Contract Contract { get; set; }
        public ContractDocument ContractDocument { get; set; }

        public ICollection<Action> Actions { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }

        public int Version { get; set; }
        public int Seq { get; set; }
    }
}
