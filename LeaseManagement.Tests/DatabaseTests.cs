using LeaseManagement.Data;
using LeaseManagement.Data.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace LeaseManagement.Tests
{
    [TestClass]
    public class DatabaseTests
    {
        [TestMethod]
        public void CanInsertContractIntoSqlDatabase()
        {
            using(var context = new ContractContext())
            {
                //Arrange
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
                var contract = new Contract();             

                //Assert 
                Assert.AreEqual(Guid.Empty, contract.Id);

                //Action
                context.Contracts.Add(contract);               
                context.SaveChanges();

                //Assert
                Assert.AreNotEqual(Guid.Empty, contract.Id);
            }
        }

        [TestMethod]
        public void CanInsertContractIntoInMemoeryDatabase()
        {
            var builder = new DbContextOptionsBuilder<ContractContext>();
            builder.UseInMemoryDatabase("CanInsertTestDb");
            using (var context = new ContractContext(builder.Options))
            {
                var contract = new Contract();

                Assert.AreEqual(Guid.Empty, contract.Id);

                context.Contracts.Add(contract);

                context.SaveChanges();
                Assert.AreNotEqual(Guid.Empty, contract.Id);
            }
        }
    }
}
