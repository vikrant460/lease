﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace LeaseManagement.Helper
{
    public interface IRestClientWrapper
    {
        public Task<TResponse> PostAsync<TRequest, TResponse>(string uri, TRequest request);
        Task<TResponse> UploadAsync<TRequest, TResponse>(string uri, TRequest request, string filePath);
        public Task<TResponse> GetAsync<TResponse>(string uri);
    }

    public class RestClientWrapper : IRestClientWrapper
    {
        private static readonly HttpClient _client = new HttpClient();
        public RestClientWrapper()
        {

        }
        public async Task<TResponse> GetAsync<TResponse>(string uri)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

                using (HttpResponseMessage response = await client.GetAsync(uri))
                {
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    return JsonConvert.DeserializeObject<TResponse>(responseBody);
                }
            }
        }

        public async Task<TResponse> PostAsync<TRequest, TResponse>(string uri, TRequest request)
        {
            _client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            var serialized = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");

            using (HttpResponseMessage response = await _client.PostAsync(uri, serialized))
            {
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<TResponse>(responseBody);
            }
        }

        public async Task<TResponse> UploadAsync<TRequest, TResponse>(string uri, TRequest request, string filePath)
        {
            MultipartFormDataContent form = new MultipartFormDataContent();
            using var fileContent = new ByteArrayContent(await File.ReadAllBytesAsync(filePath));
            fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("multipart/form-data");

            var contractJson = JsonConvert.SerializeObject(request);

            form.Add(fileContent, "files", Path.GetFileName(filePath));
            form.Add(new StringContent(contractJson), "contractDocument");

            using (HttpResponseMessage response = await _client.PostAsync(uri, form))
            {
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<TResponse>(responseBody);
            }
        }

    }
}
