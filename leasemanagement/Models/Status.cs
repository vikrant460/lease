﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaseManagement.Models
{
    public class Status : Timestamp
    {
        public int Id { get; set; }
        public StatusName Name { get; set; }
        public string DisplayName { get; set; }
    }

    public enum StatusName : int
    {
        New = 1,
        InProgress = 2,
        Completed = 3
    }
}
