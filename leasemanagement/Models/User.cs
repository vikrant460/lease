﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaseManagement.Models
{
    public class User : Timestamp
    {
        public int Id { get; set; }
        public Organization Organization { get; set; }

        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
