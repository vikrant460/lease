﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaseManagement.Models
{
    public class Action : Timestamp
    {
        public int Id { get; set; }

        public Contract Contract { get; set; }
        public ContractDocument ContractDocument { get; set; }
        public ContractScratchDocument ContractScratchDocument { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime ActionDate { get; set; }

        public Status Status { get; set; }

    }
}
