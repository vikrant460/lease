﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaseManagement.Models
{
    public class Contract : Timestamp
    {
        public Contract()
        {
            ContractDocuments = new List<ContractDocument>();
            Actions = new List<Action>();
        }

        public int Id { get; set; }
        public Organization Organization { get; set; }

        public ICollection<ContractDocument> ContractDocuments { get; set; }

        public ICollection<Action> Actions { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public Status Status { get; set; }

    }
}
