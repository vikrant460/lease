﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaseManagement.Models
{
    public class Timestamp
    {
        public DateTime CreateDate { get; set; }
        public DateTime LastUpdateDate { get; set; }

        public Guid CreatedBy { get; set; }
        public Guid LastUpdatedBy { get; set; }

        public bool IsActive { get; set; }
    }
}
