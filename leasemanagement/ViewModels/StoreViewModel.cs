﻿using LeaseManagement.Data;
using LeaseManagement.Data.Domain;
using LeaseManagement.Service;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaseManagement.ViewModels
{
    public class StoreViewModel 
    {
        public List<Contract> Contracts { get; set; }

    }
}
