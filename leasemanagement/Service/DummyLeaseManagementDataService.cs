﻿using LeaseManagement.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LeaseManagement.Service
{    
    public interface ILeaseManagementDataService
    {
        IEnumerable<Contract> GetAllContracts();
        IEnumerable<ContractDocument> GetAllContractDocuments(int ContractId);
        //Contract GetContractById(int id);
        //UploadContract(IFormFile);
    }

    public class DummyLeaseManagementDataService : ILeaseManagementDataService
    {
        public DummyLeaseManagementDataService()
        {

        }

        public IEnumerable<Contract> GetAllContracts()
        {
            try
            {
                var contractsjson = File.ReadAllText(@"JSONData\contracts.json");
                var contracts = JsonConvert.DeserializeObject<List<Contract>>(contractsjson);
                return contracts;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IEnumerable<ContractDocument> GetAllContractDocuments(int ContractId)
        {
            var contract = GetAllContracts().Where(c => c.Id == ContractId).FirstOrDefault();
            return contract.ContractDocuments;
        }
    }
}
