﻿using LeaseManagement.Helper;
using LeaseManagement.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LeaseManagement.Service
{
    public class LeaseManagementDataService : ILeaseManagementDataService
    {
        private readonly IRestClientWrapper _restWrapper;

        public LeaseManagementDataService(IRestClientWrapper restWrapper)
        {
            _restWrapper = restWrapper;
        }


        public async Task<IEnumerable<Contract>> GetAllContracts()
        {
            try
            {

                //var contractsjson = File.ReadAllText(@"JSONData\contracts.json");
                //var contracts = JsonConvert.DeserializeObject<List<Contract>>(contractsjson);
                var contracts = await _restWrapper.GetAsync<List<Contract>>("https://localhost:44365/api/contracts");
                
                return contracts;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<ContractDocument> GetAllContractDocuments(int ContractId)
        {
            var contract = GetAllContracts().Where(c => c.Id == ContractId).FirstOrDefault();
            return contract.ContractDocuments;
        }
    }

}
