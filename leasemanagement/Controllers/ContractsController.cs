﻿using LeaseManagement.Data.Domain;
using LeaseManagement.Service;
using LeaseManagement.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaseManagement.Controllers
{
    public class ContractsController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ILeaseManagementDataService _dataService;
        public ContractsController(ILogger<HomeController> logger, ILeaseManagementDataService dataService)
        {
            _logger = logger;
            _dataService = dataService;
        }

        [HttpGet("store")]
        public List<Contract> getAllContracts()
        {
            return _dataService.GetAllContracts().ToList();
        }

        public IActionResult Store()
        {
            var model = new StoreViewModel();
            model.Contracts = getAllContracts();

            return View(model);
        }

        public IActionResult UploadFile(IFormFile file)
        {
            if (Request != null)
            {

                //Confirm file was provided
                if ((file != null) && (file.Length > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    //if (CloudStorageAccount.TryParse(_config["StorageConnection"], out CloudStorageAccount storageAccount))
                    //{
                    //    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                    //    CloudBlobContainer container = blobClient.GetContainerReference("test");

                    //    bool result = await container.CreateIfNotExistsAsync();


                    //    CloudBlockBlob blockBlob = container.GetBlockBlobReference(file.FileName);

                    //    await blockBlob.UploadFromStreamAsync(file.OpenReadStream());

                    //}
                    //else
                    //{
                    //    throw new Exception("");
                    //}
                }

            }

            return RedirectToAction("Store");
        }

        public void Identify()
        {

        }


    }
}
