﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaseManagement.Controllers
{
    public class AnalyzeController : Controller
    {
        public AnalyzeController()
        {

        }

        [HttpGet("analyze")]
        public IActionResult Analyze()
        {
            return View();
        }
    }
}
