﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LeaseManagement.Controllers
{
    public class ActController : Controller
    {
        public ActController()
        {

        }

        [HttpGet("act")]
        public IActionResult Act()
        {
            return View();
        }
    }
}
